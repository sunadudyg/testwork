<?php
namespace Figures;
require_once dirname(dirname(__FILE__)).'/interface/figureInterface.php';
class Pyramid implements Figure
{
	private $c;
	private $d;
	
	
	public function __construct($c, $d)
	{
		$this->c = $c;
		$this->d = $d;
		
	}
	public function Area()
	{
		$c=$this->c;
		$d=$this->d;
		$a=pow($this->d = $d,2)-(pow($this->c,2)/4);
        if ($a<0)
        	{return 0;}
        else {
		$s=pow($this->c,2)+2*$this->c*sqrt($a);
		return $s;}
	}
}